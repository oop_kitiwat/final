/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kitiwat.finalproject;

import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author sanak
 */
public class Ultron {
    private int ultronhand; //  hand: 0:rock, 2:scissors, 1:paper
    private int playerHand; //  hand: 0:rock, 2:scissors, 1:paper
    private int win, lose, draw;
    private int status;
    
    
     public Ultron() {
         
     }
     
     private int ultronRandom() {
         return ThreadLocalRandom.current().nextInt(0, 3);
     }
     
     public int ultronYingChoob(int playerHand) {
         this.playerHand = playerHand;
         this.ultronhand = ultronRandom();
         if (this.playerHand == this.ultronhand) {
            draw++;
            status = 0;
            return 0;
        }
        if (this.playerHand == 0 && this.ultronhand == 2) {
            win++;
            status = 1;
            return 1;
        }
        if (this.playerHand == 1 && this.ultronhand == 0) {
            win++;
            status = 1;
            return 1;
        }
        if (this.playerHand == 2 && this.ultronhand == 1) {
            win++;
            status = 1;
            return 1;
        }
        lose++;
        status = -1;
        return -1;
     }
     public int getultronHand() {
        return ultronhand;
    }

    public int getPlayerHand() {
        return playerHand;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }
    public int getStatus() {
        return status;
    }
  
    
}
