/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.kitiwat.finalproject;

import java.awt.Color;
import javax.swing.JOptionPane;

/**
 *
 * @author sanak
 */
public class Game2 extends javax.swing.JFrame {

    private String startGame = "X";
    private int xCount = 0;
    private int oCount = 0;
    boolean checker;

    public Game2() {
        super("Tic Tac Toe Game");
        initComponents();
    }

    private void gameScore() {
        lblXscore.setText(String.valueOf(xCount));
        lblOscore.setText(String.valueOf(oCount));
    }

    private void choose_a_player() {
        if (startGame.equalsIgnoreCase("X")) {
            startGame = "O";
        } else {
            startGame = "X";
        }
    }

    private void winningGame() {
        String b1 = btnBox1.getText();
        String b2 = btnBox2.getText();
        String b3 = btnBox3.getText();
        String b4 = btnBox4.getText();
        String b5 = btnBox5.getText();
        String b6 = btnBox6.getText();
        String b7 = btnBox7.getText();
        String b8 = btnBox8.getText();
        String b9 = btnBox9.getText();

        if (b1 == ("X") && b2 == ("X") && b3 == ("X")) {
            JOptionPane.showMessageDialog(this, "Player X Wins", "Tic Tac Toe", JOptionPane.INFORMATION_MESSAGE);
            btnBox1.setBackground(Color.ORANGE);
            btnBox2.setBackground(Color.ORANGE);
            btnBox3.setBackground(Color.ORANGE);
            xCount++;
            gameScore();
        }
        if (b4 == ("X") && b5 == ("X") && b6 == ("X")) {
            JOptionPane.showMessageDialog(this, "Player X Wins", "Tic Tac Toe", JOptionPane.INFORMATION_MESSAGE);
            btnBox4.setBackground(Color.GREEN);
            btnBox5.setBackground(Color.GREEN);
            btnBox6.setBackground(Color.GREEN);
            xCount++;
            gameScore();
        }
        if (b7 == ("X") && b8 == ("X") && b9 == ("X")) {
            JOptionPane.showMessageDialog(this, "Player X Wins", "Tic Tac Toe", JOptionPane.INFORMATION_MESSAGE);
            btnBox7.setBackground(Color.CYAN);
            btnBox8.setBackground(Color.CYAN);
            btnBox9.setBackground(Color.CYAN);
            xCount++;
            gameScore();
        }
        if (b1 == ("X") && b4 == ("X") && b7 == ("X")) {
            JOptionPane.showMessageDialog(this, "Player X Wins", "Tic Tac Toe", JOptionPane.INFORMATION_MESSAGE);
            btnBox1.setBackground(Color.BLUE);
            btnBox4.setBackground(Color.BLUE);
            btnBox7.setBackground(Color.BLUE);
            xCount++;
            gameScore();
        }
        if (b2 == ("X") && b5 == ("X") && b8 == ("X")) {
            JOptionPane.showMessageDialog(this, "Player X Wins", "Tic Tac Toe", JOptionPane.INFORMATION_MESSAGE);
            btnBox2.setBackground(Color.YELLOW);
            btnBox5.setBackground(Color.YELLOW);
            btnBox8.setBackground(Color.YELLOW);
            xCount++;
            gameScore();
        }
        if (b3 == ("X") && b6 == ("X") && b9 == ("X")) {
            JOptionPane.showMessageDialog(this, "Player X Wins", "Tic Tac Toe", JOptionPane.INFORMATION_MESSAGE);
            btnBox3.setBackground(Color.RED);
            btnBox6.setBackground(Color.RED);
            btnBox9.setBackground(Color.RED);
            xCount++;
            gameScore();
        }
        if (b1 == ("X") && b5 == ("X") && b9 == ("X")) {
            JOptionPane.showMessageDialog(this, "Player X Wins", "Tic Tac Toe", JOptionPane.INFORMATION_MESSAGE);
            btnBox1.setBackground(Color.MAGENTA);
            btnBox5.setBackground(Color.MAGENTA);
            btnBox9.setBackground(Color.MAGENTA);
            xCount++;
            gameScore();
        }
        if (b3 == ("X") && b5 == ("X") && b7 == ("X")) {
            JOptionPane.showMessageDialog(this, "Player X Wins", "Tic Tac Toe", JOptionPane.INFORMATION_MESSAGE);
            btnBox3.setBackground(Color.PINK);
            btnBox5.setBackground(Color.PINK);
            btnBox7.setBackground(Color.PINK);
            xCount++;
            gameScore();
        }
        if (b1 == ("O") && b2 == ("O") && b3 == ("O")) {
            JOptionPane.showMessageDialog(this, "Player O Wins", "Tic Tac Toe", JOptionPane.INFORMATION_MESSAGE);
            btnBox1.setBackground(Color.ORANGE);
            btnBox2.setBackground(Color.ORANGE);
            btnBox3.setBackground(Color.ORANGE);
            oCount++;
            gameScore();
        }
        if (b4 == ("O") && b5 == ("O") && b6 == ("O")) {
            JOptionPane.showMessageDialog(this, "Player O Wins", "Tic Tac Toe", JOptionPane.INFORMATION_MESSAGE);
            btnBox4.setBackground(Color.GREEN);
            btnBox5.setBackground(Color.GREEN);
            btnBox6.setBackground(Color.GREEN);
            oCount++;
            gameScore();
        }
        if (b7 == ("O") && b8 == ("O") && b9 == ("O")) {
            JOptionPane.showMessageDialog(this, "Player O Wins", "Tic Tac Toe", JOptionPane.INFORMATION_MESSAGE);
            btnBox7.setBackground(Color.CYAN);
            btnBox8.setBackground(Color.CYAN);
            btnBox9.setBackground(Color.CYAN);
            oCount++;
            gameScore();
        }
        if (b1 == ("O") && b4 == ("O") && b7 == ("O")) {
            JOptionPane.showMessageDialog(this, "Player O Wins", "Tic Tac Toe", JOptionPane.INFORMATION_MESSAGE);
            btnBox1.setBackground(Color.BLUE);
            btnBox4.setBackground(Color.BLUE);
            btnBox7.setBackground(Color.BLUE);
            oCount++;
            gameScore();
        }
        if (b2 == ("O") && b5 == ("O") && b8 == ("O")) {
            JOptionPane.showMessageDialog(this, "Player O Wins", "Tic Tac Toe", JOptionPane.INFORMATION_MESSAGE);
            btnBox2.setBackground(Color.YELLOW);
            btnBox5.setBackground(Color.YELLOW);
            btnBox8.setBackground(Color.YELLOW);
            oCount++;
            gameScore();
        }
        if (b3 == ("O") && b6 == ("O") && b9 == ("O")) {
            JOptionPane.showMessageDialog(this, "Player O Wins", "Tic Tac Toe", JOptionPane.INFORMATION_MESSAGE);
            btnBox3.setBackground(Color.RED);
            btnBox6.setBackground(Color.RED);
            btnBox9.setBackground(Color.RED);
            oCount++;
            gameScore();
        }
        if (b1 == ("O") && b5 == ("O") && b9 == ("O")) {
            JOptionPane.showMessageDialog(this, "Player O Wins", "Tic Tac Toe", JOptionPane.INFORMATION_MESSAGE);
            btnBox1.setBackground(Color.MAGENTA);
            btnBox5.setBackground(Color.MAGENTA);
            btnBox9.setBackground(Color.MAGENTA);
            oCount++;
            gameScore();
        }
        if (b3 == ("O") && b5 == ("O") && b7 == ("O")) {
            JOptionPane.showMessageDialog(this, "Player O Wins", "Tic Tac Toe", JOptionPane.INFORMATION_MESSAGE);
            btnBox3.setBackground(Color.PINK);
            btnBox5.setBackground(Color.PINK);
            btnBox7.setBackground(Color.PINK);
            oCount++;
            gameScore();
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        btnBox1 = new javax.swing.JButton();
        btnBox2 = new javax.swing.JButton();
        btnBox3 = new javax.swing.JButton();
        btnBox4 = new javax.swing.JButton();
        btnBox5 = new javax.swing.JButton();
        btnBox6 = new javax.swing.JButton();
        btnBox7 = new javax.swing.JButton();
        btnBox8 = new javax.swing.JButton();
        btnBox9 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jButton4 = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        lblPlayerO = new javax.swing.JLabel();
        lblOscore = new javax.swing.JLabel();
        lblXscore = new javax.swing.JLabel();
        lblPlayerX2 = new javax.swing.JLabel();
        btnBack = new javax.swing.JButton();
        btnReset = new javax.swing.JButton();
        btnNewGame = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 204));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBackground(new java.awt.Color(255, 204, 204));
        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnBox1.setFont(new java.awt.Font("Leelawadee UI", 1, 48)); // NOI18N
        btnBox1.setMaximumSize(new java.awt.Dimension(200, 158));
        btnBox1.setMinimumSize(new java.awt.Dimension(200, 158));
        btnBox1.setPreferredSize(new java.awt.Dimension(200, 158));
        btnBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBox1ActionPerformed(evt);
            }
        });
        jPanel2.add(btnBox1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 110, 90));

        btnBox2.setFont(new java.awt.Font("Leelawadee UI", 1, 48)); // NOI18N
        btnBox2.setMaximumSize(new java.awt.Dimension(200, 158));
        btnBox2.setMinimumSize(new java.awt.Dimension(200, 158));
        btnBox2.setPreferredSize(new java.awt.Dimension(200, 158));
        btnBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBox2ActionPerformed(evt);
            }
        });
        jPanel2.add(btnBox2, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 20, 110, 90));

        btnBox3.setFont(new java.awt.Font("Leelawadee UI", 1, 48)); // NOI18N
        btnBox3.setMaximumSize(new java.awt.Dimension(200, 158));
        btnBox3.setMinimumSize(new java.awt.Dimension(200, 158));
        btnBox3.setPreferredSize(new java.awt.Dimension(200, 158));
        btnBox3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBox3ActionPerformed(evt);
            }
        });
        jPanel2.add(btnBox3, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 20, 110, 90));

        btnBox4.setFont(new java.awt.Font("Leelawadee UI", 1, 48)); // NOI18N
        btnBox4.setMaximumSize(new java.awt.Dimension(200, 158));
        btnBox4.setMinimumSize(new java.awt.Dimension(200, 158));
        btnBox4.setPreferredSize(new java.awt.Dimension(200, 158));
        btnBox4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBox4ActionPerformed(evt);
            }
        });
        jPanel2.add(btnBox4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 110, 90));

        btnBox5.setFont(new java.awt.Font("Leelawadee UI", 1, 48)); // NOI18N
        btnBox5.setMaximumSize(new java.awt.Dimension(200, 158));
        btnBox5.setMinimumSize(new java.awt.Dimension(200, 158));
        btnBox5.setPreferredSize(new java.awt.Dimension(200, 158));
        btnBox5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBox5ActionPerformed(evt);
            }
        });
        jPanel2.add(btnBox5, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 110, 110, 90));

        btnBox6.setFont(new java.awt.Font("Leelawadee UI", 1, 48)); // NOI18N
        btnBox6.setMaximumSize(new java.awt.Dimension(200, 158));
        btnBox6.setMinimumSize(new java.awt.Dimension(200, 158));
        btnBox6.setPreferredSize(new java.awt.Dimension(200, 158));
        btnBox6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBox6ActionPerformed(evt);
            }
        });
        jPanel2.add(btnBox6, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 110, 110, 90));

        btnBox7.setFont(new java.awt.Font("Leelawadee UI", 1, 48)); // NOI18N
        btnBox7.setMaximumSize(new java.awt.Dimension(200, 158));
        btnBox7.setMinimumSize(new java.awt.Dimension(200, 158));
        btnBox7.setPreferredSize(new java.awt.Dimension(200, 158));
        btnBox7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBox7ActionPerformed(evt);
            }
        });
        jPanel2.add(btnBox7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 200, 110, 90));

        btnBox8.setFont(new java.awt.Font("Leelawadee UI", 1, 48)); // NOI18N
        btnBox8.setMaximumSize(new java.awt.Dimension(200, 158));
        btnBox8.setMinimumSize(new java.awt.Dimension(200, 158));
        btnBox8.setPreferredSize(new java.awt.Dimension(200, 158));
        btnBox8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBox8ActionPerformed(evt);
            }
        });
        jPanel2.add(btnBox8, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 200, 110, 90));

        btnBox9.setFont(new java.awt.Font("Leelawadee UI", 1, 48)); // NOI18N
        btnBox9.setMaximumSize(new java.awt.Dimension(200, 158));
        btnBox9.setMinimumSize(new java.awt.Dimension(200, 158));
        btnBox9.setPreferredSize(new java.awt.Dimension(200, 158));
        btnBox9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBox9ActionPerformed(evt);
            }
        });
        jPanel2.add(btnBox9, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 200, 110, 90));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 240, 370, 300));

        jPanel3.setBackground(new java.awt.Color(204, 204, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel4.setBackground(new java.awt.Color(204, 204, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton4.setFont(new java.awt.Font("ROG Fonts", 0, 14)); // NOI18N
        jButton4.setText("Back");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(755, 390, 99, -1));

        jPanel3.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 40, 360, 110));

        jPanel5.setBackground(new java.awt.Color(204, 204, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblPlayerO.setFont(new java.awt.Font("ROG Fonts", 0, 24)); // NOI18N
        lblPlayerO.setText("Player O :");
        jPanel5.add(lblPlayerO, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, -1, 80));

        lblOscore.setBackground(new java.awt.Color(255, 255, 255));
        lblOscore.setFont(new java.awt.Font("ROG Fonts", 0, 24)); // NOI18N
        lblOscore.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblOscore.setText("0");
        lblOscore.setOpaque(true);
        jPanel5.add(lblOscore, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 160, 170, 40));

        lblXscore.setBackground(new java.awt.Color(255, 255, 255));
        lblXscore.setFont(new java.awt.Font("ROG Fonts", 0, 24)); // NOI18N
        lblXscore.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblXscore.setText("0");
        lblXscore.setOpaque(true);
        jPanel5.add(lblXscore, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 50, 170, 40));

        lblPlayerX2.setFont(new java.awt.Font("ROG Fonts", 0, 24)); // NOI18N
        lblPlayerX2.setText("Player X :");
        jPanel5.add(lblPlayerX2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, -1, 100));

        jPanel3.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 380, 220));

        btnBack.setBackground(new java.awt.Color(255, 204, 51));
        btnBack.setFont(new java.awt.Font("ROG Fonts", 0, 14)); // NOI18N
        btnBack.setText("BACK");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        jPanel3.add(btnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 250, 100, 30));

        btnReset.setBackground(new java.awt.Color(255, 204, 51));
        btnReset.setFont(new java.awt.Font("ROG Fonts", 0, 12)); // NOI18N
        btnReset.setText("RESET");
        btnReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetActionPerformed(evt);
            }
        });
        jPanel3.add(btnReset, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 250, 110, 30));

        btnNewGame.setBackground(new java.awt.Color(255, 204, 51));
        btnNewGame.setFont(new java.awt.Font("ROG Fonts", 0, 12)); // NOI18N
        btnNewGame.setText("NEW GAME");
        btnNewGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewGameActionPerformed(evt);
            }
        });
        jPanel3.add(btnNewGame, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 250, 120, 30));

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 240, 400, 300));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon("C:\\Users\\sanak\\OneDrive\\เดสก์ท็อป\\Final work\\toe.gif")); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 830, 220));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 864, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 557, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton4ActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        FinGame back = new FinGame();
        back.setVisible(true);
        setVisible(false);
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetActionPerformed

        btnBox1.setEnabled(true);
        btnBox2.setEnabled(true);
        btnBox3.setEnabled(true);
        btnBox4.setEnabled(true);
        btnBox5.setEnabled(true);
        btnBox6.setEnabled(true);
        btnBox7.setEnabled(true);
        btnBox8.setEnabled(true);
        btnBox9.setEnabled(true);

        lblXscore.setText("0");
        lblOscore.setText("0");

        btnBox1.setText("");
        btnBox2.setText("");
        btnBox3.setText("");
        btnBox4.setText("");
        btnBox5.setText("");
        btnBox6.setText("");
        btnBox7.setText("");
        btnBox8.setText("");
        btnBox9.setText("");

        btnBox1.setBackground(Color.LIGHT_GRAY);
        btnBox2.setBackground(Color.LIGHT_GRAY);
        btnBox3.setBackground(Color.LIGHT_GRAY);
        btnBox4.setBackground(Color.LIGHT_GRAY);
        btnBox5.setBackground(Color.LIGHT_GRAY);
        btnBox6.setBackground(Color.LIGHT_GRAY);
        btnBox7.setBackground(Color.LIGHT_GRAY);
        btnBox8.setBackground(Color.LIGHT_GRAY);
        btnBox9.setBackground(Color.LIGHT_GRAY);


    }//GEN-LAST:event_btnResetActionPerformed

    private void btnBox6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBox6ActionPerformed
        btnBox6.setText(startGame);
        if (startGame.equalsIgnoreCase("X")) {
            checker = false;
        } else {
            checker = true;
        }
        choose_a_player();
        winningGame();
    }//GEN-LAST:event_btnBox6ActionPerformed

    private void btnNewGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewGameActionPerformed
        btnBox1.setEnabled(true);
        btnBox2.setEnabled(true);
        btnBox3.setEnabled(true);
        btnBox4.setEnabled(true);
        btnBox5.setEnabled(true);
        btnBox6.setEnabled(true);
        btnBox7.setEnabled(true);
        btnBox8.setEnabled(true);
        btnBox9.setEnabled(true);

        btnBox1.setText("");
        btnBox2.setText("");
        btnBox3.setText("");
        btnBox4.setText("");
        btnBox5.setText("");
        btnBox6.setText("");
        btnBox7.setText("");
        btnBox8.setText("");
        btnBox9.setText("");

        btnBox1.setBackground(Color.LIGHT_GRAY);
        btnBox2.setBackground(Color.LIGHT_GRAY);
        btnBox3.setBackground(Color.LIGHT_GRAY);
        btnBox4.setBackground(Color.LIGHT_GRAY);
        btnBox5.setBackground(Color.LIGHT_GRAY);
        btnBox6.setBackground(Color.LIGHT_GRAY);
        btnBox7.setBackground(Color.LIGHT_GRAY);
        btnBox8.setBackground(Color.LIGHT_GRAY);
        btnBox9.setBackground(Color.LIGHT_GRAY);
    }//GEN-LAST:event_btnNewGameActionPerformed

    private void btnBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBox1ActionPerformed
        btnBox1.setText(startGame);
        if (startGame.equalsIgnoreCase("X")) {
            checker = false;
        } else {
            checker = true;
        }
        choose_a_player();
        winningGame();
    }//GEN-LAST:event_btnBox1ActionPerformed

    private void btnBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBox2ActionPerformed
        btnBox2.setText(startGame);
        if (startGame.equalsIgnoreCase("X")) {
            checker = false;
        } else {
            checker = true;
        }
        choose_a_player();
        winningGame();
    }//GEN-LAST:event_btnBox2ActionPerformed

    private void btnBox3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBox3ActionPerformed
        btnBox3.setText(startGame);
        if (startGame.equalsIgnoreCase("X")) {
            checker = false;
        } else {
            checker = true;
        }
        choose_a_player();
        winningGame();
    }//GEN-LAST:event_btnBox3ActionPerformed

    private void btnBox4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBox4ActionPerformed
        btnBox4.setText(startGame);
        if (startGame.equalsIgnoreCase("X")) {
            checker = false;
        } else {
            checker = true;
        }
        choose_a_player();
        winningGame();
    }//GEN-LAST:event_btnBox4ActionPerformed

    private void btnBox5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBox5ActionPerformed
        btnBox5.setText(startGame);
        if (startGame.equalsIgnoreCase("X")) {
            checker = false;
        } else {
            checker = true;
        }
        choose_a_player();
        winningGame();
    }//GEN-LAST:event_btnBox5ActionPerformed

    private void btnBox7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBox7ActionPerformed
        btnBox7.setText(startGame);
        if (startGame.equalsIgnoreCase("X")) {
            checker = false;
        } else {
            checker = true;
        }
        choose_a_player();
        winningGame();
    }//GEN-LAST:event_btnBox7ActionPerformed

    private void btnBox8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBox8ActionPerformed
        btnBox8.setText(startGame);
        if (startGame.equalsIgnoreCase("X")) {
            checker = false;
        } else {
            checker = true;
        }
        choose_a_player();
        winningGame();
    }//GEN-LAST:event_btnBox8ActionPerformed

    private void btnBox9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBox9ActionPerformed
        btnBox9.setText(startGame);
        if (startGame.equalsIgnoreCase("X")) {
            checker = false;
        } else {
            checker = true;
        }
        choose_a_player();
        winningGame();
    }//GEN-LAST:event_btnBox9ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Game2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Game2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Game2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Game2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Game2().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnBox1;
    private javax.swing.JButton btnBox2;
    private javax.swing.JButton btnBox3;
    private javax.swing.JButton btnBox4;
    private javax.swing.JButton btnBox5;
    private javax.swing.JButton btnBox6;
    private javax.swing.JButton btnBox7;
    private javax.swing.JButton btnBox8;
    private javax.swing.JButton btnBox9;
    private javax.swing.JButton btnNewGame;
    private javax.swing.JButton btnReset;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JLabel lblOscore;
    private javax.swing.JLabel lblPlayerO;
    private javax.swing.JLabel lblPlayerX2;
    private javax.swing.JLabel lblXscore;
    // End of variables declaration//GEN-END:variables
}
